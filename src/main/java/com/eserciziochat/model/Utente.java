package com.eserciziochat.model;

import java.util.ArrayList;

public class Utente {
	
	private int id;
	private String nome;
	private String nickname;
	private String cognome;
	private String email;
	private String password;
	
	ArrayList <String> messaggi=new ArrayList <String>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void aggiungiMess(String Messaggio) {
		
		messaggi.add(Messaggio);
		
	}
	
	
	
	


}
