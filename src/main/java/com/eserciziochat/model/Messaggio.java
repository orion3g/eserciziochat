package com.eserciziochat.model;

public class Messaggio {
	
	
	private int idMessaggio;
	private int utente_ref;
	private String messaggio;
	
	
	
	public int getIdMessaggio() {
		return idMessaggio;
	}
	public void setIdMessaggio(int idMessaggio) {
		this.idMessaggio = idMessaggio;
	}
	public int getUtente_ref() {
		return utente_ref;
	}
	public void setUtente_ref(int utente_ref) {
		this.utente_ref = utente_ref;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	
	
	
	
	
	

}
