package com.eserciziochat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.eserciziochat.connection.ConnettoreDB;
import com.eserciziochat.model.Utente;

public class UtenteDao implements Dao<Utente> {

    public Utente getByNick(String nick) throws SQLException {
        Utente temp = new Utente();
        Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "SELECT id,nome,cognome,nickname,email.passw FROM utenti WHERE nickname=?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, nick);

        ResultSet risultato = ps.executeQuery();
        risultato.next();
        temp.setId(risultato.getInt(1));
        temp.setNome(risultato.getString(2));
        temp.setCognome(risultato.getString(3));
        temp.setNickname(risultato.getString(4));
        temp.setEmail(risultato.getString(5));
        temp.setPassword(risultato.getString(6));

        return temp;
    }

    @Override
    public Utente getById(Integer id) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id,nome,cognome,nickname,email,passw FROM utenti WHERE id=?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet risultato = ps.executeQuery();
        risultato.next();

        Utente temp = new Utente();
        temp.setId(risultato.getInt(1));
        temp.setNome(risultato.getString(2));
        temp.setCognome(risultato.getString(3));
        temp.setNickname(risultato.getString(4));
        temp.setEmail(risultato.getString(5));
        temp.setPassword(risultato.getString(6));

        return temp;
    }

    @Override
    public ArrayList<Utente> getAll() throws SQLException {
        ArrayList<Utente> elenco = new ArrayList<Utente>();

        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id,nome,cognome,nickname,email,passw FROM utenti";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

        ResultSet risultato = ps.executeQuery();
        while (risultato.next()) {
            Utente temp = new Utente();
            temp.setId(risultato.getInt(1));
            temp.setNome(risultato.getString(2));
            temp.setCognome(risultato.getString(3));
            temp.setNickname(risultato.getString(4));
            temp.setEmail(risultato.getString(5));
            temp.setPassword(risultato.getString(6));
            elenco.add(temp);
        }

        return elenco;
    }

    @Override
    public void insert(Utente t) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();


        String query = "INSERT INTO utenti (nome,cognome,nickname,email,passw) VALUES (?,?,?,?,?)";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, t.getNome());
        ps.setString(2, t.getCognome());
        ps.setString(3, t.getNickname());
        ps.setString(4, t.getEmail());
        ps.setString(5, t.getPassword());

        ps.executeUpdate();
        ResultSet risultato = ps.getGeneratedKeys();
        risultato.next();

        t.setId(risultato.getInt(1));

    }

    @Override
    public boolean delete(Utente t) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "DELETE FROM utenti WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, t.getId());

        int affRows = ps.executeUpdate();
        if (affRows > 0)
            return true;

        return false;
    }

    @Override
    
    public Utente update(Utente t) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "UPDATE utenti SET " + "nome = ?, " + "cognome = ?, " + "nickname = ?, " + "email = ? "
                + "passw= ? " + "WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setString(1, t.getNome());
        ps.setString(2, t.getCognome());
        ps.setString(3, t.getNickname());
        ps.setString(4, t.getEmail());
        ps.setString(5, t.getPassword());
        ps.setInt(6, t.getId());

        int affRows = ps.executeUpdate();
        if (affRows > 0)
            return getById(t.getId());

        return null;
    }


    
  
    
    
    

}