package com.eserciziochat.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.eserciziochat.connection.ConnettoreDB;
import com.eserciziochat.model.Messaggio;
import com.eserciziochat.model.id_nick;

public class MessaggiDao implements Dao<Messaggio> {

	@Override
	public Messaggio getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT idMessaggio,utente_ref,messaggio FROM messaggi WHERE id=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Messaggio temp = new Messaggio();
		temp.setIdMessaggio(risultato.getInt(1));
		temp.setUtente_ref(risultato.getInt(2));
		temp.setMessaggio(risultato.getString(3));

		return temp;
	}



	@Override
	public void insert(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO messaggi (utente_ref,messaggio) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, t.getUtente_ref());
		ps.setString(2, t.getMessaggio());

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();

		t.setIdMessaggio(risultato.getInt(1));

	}

	@Override
	public boolean delete(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM messaggi WHERE id = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getIdMessaggio());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	@Override
	public Messaggio update(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE messaggi SET mesaggio=? WHERE idMessaggio=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getMessaggio());
		ps.setInt(2, t.getIdMessaggio());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getIdMessaggio());

		return null;
	}

	
    public ArrayList<String> getAll1() throws SQLException {
		
        ArrayList <String> messaggi= new ArrayList<String>();

        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT nickname, messaggio FROM messaggi JOIN utenti ON messaggi.utente_ref=utenti.id;";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

        ResultSet risultato = ps.executeQuery();
        
        String nickname;
        String messaggio;
        
        String ris="";
        
        
        while (risultato.next()) {
            
            nickname=risultato.getString(1);
            messaggio=risultato.getString(2);
           
            ris="{"+"nickname:"+nickname+","+ "messaggio:"+messaggio+"}";
            messaggi.add(ris);
            
        }

        return messaggi;
    }



	@Override
	public ArrayList<Messaggio> getAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	



}