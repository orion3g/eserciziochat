package com.eserciziochat.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public interface Dao <T>{


    T getById(Integer id) throws SQLException;

    ArrayList<T> getAll() throws SQLException;

  
    void insert(T t) throws SQLException;

    boolean delete(T t) throws SQLException;

    T update(T t) throws SQLException;

}