package com.eserciziochat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EserciziochatApplication {

	public static void main(String[] args) {
		SpringApplication.run(EserciziochatApplication.class, args);
	}

}
