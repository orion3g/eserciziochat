package com.eserciziochat.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eserciziochat.dao.MessaggiDao;
import com.eserciziochat.dao.UtenteDao;
import com.eserciziochat.model.Messaggio;
import com.eserciziochat.model.Utente;

@CrossOrigin ("http://localhost:8080")
@RestController
public class MessaggiController {

//	  @GetMapping("/list")
//      public ArrayList<Messaggio> restituisciTuttiConsumabili(){
//          ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
//
//          MessaggiDao conDao = new MessaggiDao();
//          try {
//              elenco = conDao.getAll();
//          } catch (SQLException e) {
//              System.out.println(e.getMessage());
//          }
//          return elenco;
//      }

	@PostMapping("/insertmessaggio")
	void inserisciConsumabile(HttpServletRequest request, HttpServletRequest response) {

		HttpSession sessione = request.getSession();

		String messaggio = request.getParameter("messaggio");

		Utente temp;
		temp = (Utente) sessione.getAttribute("varuse");

		UtenteDao ud = new UtenteDao();
		ArrayList<Utente> elenco = new ArrayList<Utente>();

		try {
			elenco = ud.getAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Utente utente = null;

		for (int i = 0; i < elenco.size(); i++) {

			if (elenco.get(i).getNickname().equals(temp.getNickname())) {

				utente = elenco.get(i);
				utente.aggiungiMess(messaggio);

			}
		}

		Messaggio ms = new Messaggio();
		ms.setMessaggio(messaggio);
		ms.setUtente_ref(utente.getId());

		MessaggiDao md = new MessaggiDao();
		try {
			md.insert(ms);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@PostMapping("/stampamessaggi")

	public ArrayList<String> visualizzamessaggi() {


		 ArrayList <String> messaggi = new ArrayList<String>();


		MessaggiDao ms = new MessaggiDao();

		try {
			 
			messaggi=ms.getAll1();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return messaggi;

	}

}
