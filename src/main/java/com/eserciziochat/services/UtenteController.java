package com.eserciziochat.services;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eserciziochat.dao.UtenteDao;
import com.eserciziochat.model.Utente;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@CrossOrigin ("http://localhost:8080")
@RequestMapping("/utente")
public class UtenteController {

	@PostMapping("/nuovo")
	public Utente inserisciUtente(@RequestBody Utente objUte) {

		UtenteDao uteDao = new UtenteDao();

		try {
			uteDao.insert(objUte);
			return objUte;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

	
	
	@PostMapping("/login")
	public int Login(HttpServletRequest request, HttpServletRequest response) {

		String var_username = request.getParameter("nickname");
		String var_password = request.getParameter("password");

		HttpSession sessione = request.getSession();

		if (sessione.getAttribute("isAuth") != null && (boolean) sessione.getAttribute("isAuth") == true) {

			System.out.println("già c'eri");
			return 1; //sessione già attiva indirizzi alla chat
		}
		
		UtenteDao ud= new UtenteDao();
		ArrayList <Utente> elenco=new ArrayList<Utente>();
		try {
			elenco=ud.getAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i=0; i<elenco.size(); i++) {
		
			if (elenco.get(i).getNickname().equals(var_username)  && (elenco.get(i).getPassword().equals(var_password) )) {
			sessione.setAttribute("isAuth", true);

			System.out.println("sei entrato");
			
			sessione.setAttribute("varuse", elenco.get(i));
			
			return 2; //controllo login okk mi vai alla chat
		}}

		System.out.println("non sei entrato");
			return 3; // controllo login non ok vai alla pagina di errore pass o nick non esatti
		

	}
		

	
	
	

}
